package businessLayer;

import dao.UniversalDAO;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import model.Client;
import model.Produs;

import java.io.IOException;
import java.util.ArrayList;

/**
 * @Author: Costel Corbuleac
 * @Since: Jun 06, 2019
 */

public class ProdusController {
    public HBox insertPlace;

    private HBox addHBox(String s, String s2) {
        HBox newH = new HBox();
        newH.getChildren().add(new Label(s));
        TextField newT = new TextField();
        newT.setPromptText(s2);
        newH.getChildren().add(newT);
        return newH;
    }

    public VBox addFields() {
        if (insertPlace.getChildren().size() > 0)
            insertPlace.getChildren().remove(0);

        VBox toInsert = new VBox();
        toInsert.getStyleClass().add("input-container");

        HBox newH = addHBox("Product ID:", "Introduce ID");

        HBox newH1 = addHBox("Product Price:", "Introduce Price");

        HBox newH2 = addHBox("Product Name:", "Introduce Name");

        HBox newH3 = addHBox("Product Quantity:", "Introduce Quantity");

        toInsert.getChildren().add(newH);
        toInsert.getChildren().add(newH1);
        toInsert.getChildren().add(newH2);
        toInsert.getChildren().add(newH3);

        return toInsert;
    }

    public void handleAdd() {
        VBox toInsert = addFields();

        HBox newH4 = new HBox();
        Button B1 = new Button();
        B1.setText("Submit Data");
        B1.setOnAction(e -> {
            handleAddList();
        });
        newH4.getChildren().add(B1);

        toInsert.getChildren().add(newH4);
        insertPlace.getChildren().add(toInsert);
    }

    private void handleAddList() {
        System.out.println("Been Here");
        productD productD = new productD().invoke();

        UniversalDAO.insertObject(new Produs(productD.getId_produs(), productD.getPret(), productD.getNume(), productD.getCantitate()));
    }

    public void handleEdit() {
        VBox toInsert = addFields();

        HBox newH4 = new HBox();
        Button B1 = new Button();
        B1.setText("Edit Data");
        B1.setOnAction(e -> {
            handleEditList();
        });
        newH4.getChildren().add(B1);

        toInsert.getChildren().add(newH4);
        insertPlace.getChildren().add(toInsert);
    }

    private void handleEditList() {
        System.out.println("Been Here");
        productD productD = new productD().invoke();

        ArrayList<Integer> id = new ArrayList<>();
        id.add(productD.getId_produs());
        UniversalDAO.updateObject(new Produs(productD.getId_produs(), productD.getPret(), productD.getNume(), productD.getCantitate()), id);
    }

    public void handleDelete() {
        if (insertPlace.getChildren().size() > 0)
            insertPlace.getChildren().remove(0);

        VBox toInsert = new VBox();
        toInsert.getStyleClass().add("input-container");

        HBox newH = addHBox("Product ID:", "Introduce ID");

        HBox newH4 = new HBox();
        Button B1 = new Button();
        B1.setText("Delete Data");
        B1.setOnAction(e -> {
            handleDeleteList();
        });
        newH4.getChildren().add(B1);

        toInsert.getChildren().add(newH);
        toInsert.getChildren().add(newH4);
        insertPlace.getChildren().add(toInsert);
    }

    private void handleDeleteList() {
        System.out.println("Delete this");
        ArrayList<TextField> fields = getTextFields(1);

        int id = Integer.parseInt(fields.get(0).getText());
        ArrayList<Integer> ids = new ArrayList<>();
        ids.add(id);

        UniversalDAO.deleteById(new Produs(), ids);
    }

    public void handleView() {

        if (insertPlace.getChildren().size() > 0)
            insertPlace.getChildren().remove(0);

        FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/produsTable.fxml"));
        Stage newStage = new Stage();
        Parent root1 = null;
        try {
            root1 = loader.load();
        } catch (IOException e) {
            e.printStackTrace();
        }
        root1.getStylesheets().add(getClass().getResource("/css/main.css").toExternalForm());
        newStage.setScene(new Scene(root1, 800, 600));
        newStage.setTitle("Product Table");
        newStage.show();

    }

    private ArrayList<TextField> getTextFields(int nr) {
        ArrayList<TextField> fields = new ArrayList<>();
        for (int i = 0; i < nr; i++) {
            fields.add((TextField) (((HBox) ((VBox) insertPlace.getChildren().get(0)).getChildren().get(i)).getChildren().get(1)));
        }
        return fields;
    }

    private class productD {
        private int id_produs;
        private int pret;
        private int cantitate;
        private String nume;

        public int getCantitate() {
            return cantitate;
        }

        public int getId_produs() {
            return id_produs;
        }

        public int getPret() {
            return pret;
        }

        public String getNume() {
            return nume;
        }


        public productD invoke() {
            ArrayList<TextField> fields = getTextFields(4);
            id_produs = Integer.parseInt(fields.get(0).getText());
            pret = Integer.parseInt(fields.get(1).getText());
            nume = fields.get(2).getText();
            cantitate = Integer.parseInt(fields.get(3).getText());

            return this;
        }
    }
}
