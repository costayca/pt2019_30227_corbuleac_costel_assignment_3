package businessLayer;

import dao.UniversalDAO;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import model.Iesire;
import model.Produs;

import java.io.IOException;
import java.util.ArrayList;

/**
 * @Author: Costel Corbuleac
 * @Since: Jun 06, 2019
 */

public class IesireController {
    public HBox insertPlace;

    private void invalidQuantity() {
        Alert alert = new Alert(Alert.AlertType.ERROR);
        alert.setTitle("Error");
        alert.setHeaderText("Invalid number of products");
        alert.setContentText(String.format("The store doesn't own that many items!!"));

        alert.showAndWait();
    }

    private HBox addHBox(String s, String s2) {
        HBox newH = new HBox();
        newH.getChildren().add(new Label(s));
        TextField newT = new TextField();
        newT.setPromptText(s2);
        newH.getChildren().add(newT);
        return newH;
    }

    public VBox addFields() {
        if (insertPlace.getChildren().size() > 0)
            insertPlace.getChildren().remove(0);

        VBox toInsert = new VBox();
        toInsert.getStyleClass().add("input-container");

        HBox newH = addHBox("Order ID:", "Introduce ID");

        HBox newH1 = addHBox("Product ID:", "Introduce Product ID");

        HBox newH2 = addHBox("Product Quantity:", "Introduce Product Quantity");

        toInsert.getChildren().add(newH);
        toInsert.getChildren().add(newH1);
        toInsert.getChildren().add(newH2);

        return toInsert;
    }

    public void handleAdd() {
        VBox toInsert = addFields();

        HBox newH4 = new HBox();
        Button B1 = new Button();
        B1.setText("Submit Data");
        B1.setOnAction(e -> {
            handleAddList();
        });
        newH4.getChildren().add(B1);

        toInsert.getChildren().add(newH4);
        insertPlace.getChildren().add(toInsert);
    }

    private void handleAddList() {
        System.out.println("Been Here");
        DetailD DetailD = new DetailD().invoke();

        ArrayList<Integer> idP = new ArrayList<>();
        idP.add(DetailD.getId_produs());
        Produs p = (Produs) UniversalDAO.findById(new Produs(), idP);

        if (p != null && p.getCantitate() - DetailD.getCantitate() >= 0) {
            UniversalDAO.updateObject(new Produs(p.getId_produs(), p.getPret(), p.getNume(), p.getCantitate() - DetailD.getCantitate()), idP);
            UniversalDAO.insertObject(new Iesire(DetailD.getId_comanda(), DetailD.getId_produs(), DetailD.getCantitate()));
        } else {
            invalidQuantity();
        }
    }

    public void handleEdit() {
        VBox toInsert = addFields();

        HBox newH4 = new HBox();
        Button B1 = new Button();
        B1.setText("Edit Data");
        B1.setOnAction(e -> {
            handleEditList();
        });
        newH4.getChildren().add(B1);

        toInsert.getChildren().add(newH4);
        insertPlace.getChildren().add(toInsert);
    }

    private void handleEditList() {
        System.out.println("Been Here");
        DetailD DetailD = new DetailD().invoke();

        ArrayList<Integer> id = new ArrayList<>();
        id.add(DetailD.getId_comanda());
        id.add(DetailD.getId_produs());

        Iesire i = (Iesire) UniversalDAO.findById(new Iesire(), id);

        ArrayList<Integer> idP = new ArrayList<>();
        idP.add(DetailD.getId_produs());
        Produs p = (Produs) UniversalDAO.findById(new Produs(), idP);

        if (i != null && p != null && p.getCantitate() - DetailD.getCantitate() + i.getCantitate() >= 0) {
            UniversalDAO.updateObject(new Produs(p.getId_produs(), p.getPret(), p.getNume(), p.getCantitate() - DetailD.getCantitate() + i.getCantitate()), idP);
            UniversalDAO.updateObject(new Iesire(DetailD.getId_comanda(), DetailD.getId_produs(), DetailD.getCantitate()), id);
        } else {
            invalidQuantity();
        }
    }

    public void handleDelete() {
        if (insertPlace.getChildren().size() > 0)
            insertPlace.getChildren().remove(0);

        VBox toInsert = new VBox();
        toInsert.getStyleClass().add("input-container");

        HBox newH = addHBox("Order ID:", "Introduce ID");
        HBox newH2 = addHBox("Product ID:", "Introduce Product ID");

        HBox newH4 = new HBox();
        Button B1 = new Button();
        B1.setText("Delete Data");
        B1.setOnAction(e -> {
            handleDeleteList();
        });
        newH4.getChildren().add(B1);

        toInsert.getChildren().add(newH);
        toInsert.getChildren().add(newH2);
        toInsert.getChildren().add(newH4);
        insertPlace.getChildren().add(toInsert);
    }

    private void handleDeleteList() {
        System.out.println("Delete this");
        ArrayList<TextField> fields = getTextFields(2);

        int id = Integer.parseInt(fields.get(0).getText());
        int id1 = Integer.parseInt(fields.get(1).getText());
        ArrayList<Integer> ids = new ArrayList<>();
        ids.add(id);
        ids.add(id1);

        Iesire i = (Iesire) UniversalDAO.findById(new Iesire(), ids);

        ArrayList<Integer> idP = new ArrayList<>();
        idP.add(id1);
        Produs p = (Produs) UniversalDAO.findById(new Produs(), idP);

        if (i != null && p != null) {
            UniversalDAO.updateObject(new Produs(p.getId_produs(), p.getPret(), p.getNume(), p.getCantitate() + i.getCantitate()), idP);
        }

        UniversalDAO.deleteById(new Iesire(), ids);
    }

    public void handleView() {

        if (insertPlace.getChildren().size() > 0)
            insertPlace.getChildren().remove(0);

        FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/iesireTable.fxml"));
        Stage newStage = new Stage();
        Parent root1 = null;
        try {
            root1 = loader.load();
        } catch (IOException e) {
            e.printStackTrace();
        }
        root1.getStylesheets().add(getClass().getResource("/css/main.css").toExternalForm());
        newStage.setScene(new Scene(root1, 800, 600));
        newStage.setTitle("Order Details Table");
        newStage.show();

    }

    private ArrayList<TextField> getTextFields(int nr) {
        ArrayList<TextField> fields = new ArrayList<>();
        for (int i = 0; i < nr; i++) {
            fields.add((TextField) (((HBox) ((VBox) insertPlace.getChildren().get(0)).getChildren().get(i)).getChildren().get(1)));
        }
        return fields;
    }

    private class DetailD {
        private int id_comanda;
        private int id_produs;
        private int cantitate;

        public int getCantitate() {
            return cantitate;
        }

        public int getId_comanda() {
            return id_comanda;
        }

        public int getId_produs() {
            return id_produs;
        }


        public DetailD invoke() {
            ArrayList<TextField> fields = getTextFields(3);
            id_comanda = Integer.parseInt(fields.get(0).getText());
            id_produs = Integer.parseInt(fields.get(1).getText());
            cantitate = Integer.parseInt(fields.get(2).getText());

            return this;
        }
    }
}
