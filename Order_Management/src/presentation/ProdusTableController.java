package presentation;

import dao.UniversalDAO;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.TableView;
import javafx.scene.layout.VBox;
import model.Produs;

/**
 * @Author: Costel Corbuleac
 * @Since: Jun 06, 2019
 */

public class ProdusTableController {
    @FXML
    private VBox container;

    @FXML
    public void initialize() {
        TableView<Object> table = new TableView<>();

        ObservableList<Object> list = UniversalDAO.viewAll(new Produs(), table);

        table.setColumnResizePolicy(TableView.CONSTRAINED_RESIZE_POLICY);
        table.setItems(list);
        container.getChildren().add(table);
    }

}
