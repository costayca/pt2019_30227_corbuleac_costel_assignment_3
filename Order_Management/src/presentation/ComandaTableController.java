package presentation;

import dao.UniversalDAO;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import model.Client;
import model.Comanda;
import model.Iesire;
import model.Produs;

import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * @Author: Costel Corbuleac
 * @Since: Jun 06, 2019
 */

public class ComandaTableController {
    @FXML
    private VBox container;

    @FXML
    public HBox insertPlace;

    @FXML
    public void initialize() {
        TableView<Object> table = new TableView<>();

        ObservableList<Object> list = UniversalDAO.viewAll(new Comanda(), table);

        table.setColumnResizePolicy(TableView.CONSTRAINED_RESIZE_POLICY);
        table.setItems(list);
        container.getChildren().add(table);
        handleCreate();
    }

    private HBox addHBox(String s, String s2) {
        HBox newH = new HBox();
        newH.getChildren().add(new Label(s));
        TextField newT = new TextField();
        newT.setPromptText(s2);
        newH.getChildren().add(newT);
        return newH;
    }

    public VBox addFields() {
        if (insertPlace.getChildren().size() > 0)
            insertPlace.getChildren().remove(0);

        VBox toInsert = new VBox();
        toInsert.getStyleClass().add("input-container");

        HBox newH = addHBox("Order ID:", "Introduce ID");

        toInsert.getChildren().add(newH);

        return toInsert;
    }

    public void handleCreate() {
        VBox toInsert = addFields();

        HBox newH4 = new HBox();
        Button B1 = new Button();
        B1.setText("Create Order Bill");
        B1.setOnAction(e -> {
            handleCreateBill();
        });
        newH4.getChildren().add(B1);

        toInsert.getChildren().add(newH4);
        insertPlace.getChildren().add(toInsert);
    }

    private ArrayList<TextField> getTextFields(int nr) {
        ArrayList<TextField> fields = new ArrayList<>();
        for (int i = 0; i < nr; i++) {
            fields.add((TextField) (((HBox) ((VBox) insertPlace.getChildren().get(0)).getChildren().get(i)).getChildren().get(1)));
        }
        return fields;
    }

    private void handleCreateBill() {
        ArrayList<TextField> fields = getTextFields(1);
        int id = Integer.parseInt(fields.get(0).getText());
        try {
            FileWriter writer = new FileWriter("Order" + id + ".txt");
            int price = 0;
            ArrayList<Integer> ids = new ArrayList<>();
            ids.add(id);
            Comanda c = (Comanda) UniversalDAO.findById(new Comanda(), ids);
            ids.remove(0);
            List<Iesire> iesiri = UniversalDAO.selectByIdIesire(id);
            List<Produs> produse = new ArrayList<>();
            for (Iesire input : iesiri) {
                ids.add(input.getId_produs());
                Produs p = (Produs) UniversalDAO.findById(new Produs(), ids);
                price += p.getPret() * input.getCantitate();
                produse.add(p);
                ids.remove(0);
            }
            ids.add(c.getId_client());
            Client client = (Client) UniversalDAO.findById(new Client(), ids);
            ids.remove(0);
            String write = "Detalii: " + iesiri.toString() + "\nClient: " + client + "\nProduse: " + produse.toString() + "\nTotal: " + price;
            writer.write(write);
            writer.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
