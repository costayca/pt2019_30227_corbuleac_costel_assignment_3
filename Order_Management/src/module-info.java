module Order.Management {
    requires javafx.fxml;
    requires javafx.controls;
    requires java.sql;

    opens model;
    opens presentation;
    opens businessLayer;
    opens start;
    opens dao;
//    opens dataAccessLayer;
//    opens businessLayer;
}