package dao;

import connection.ConnectionFactory;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import model.*;

import java.lang.reflect.Field;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * @Author: Costel Corbuleac
 * @Since: Jun 06, 2019
 */

public class UniversalDAO {

    protected static final Logger LOGGER = Logger.getLogger(UniversalDAO.class.getName());

    public static int insertObject(Object object) {
        Connection dbConnection = ConnectionFactory.getConnection();
        PreparedStatement insertStatement = null;

        String insertStatementString = "INSERT INTO " + object.getClass().getSimpleName() + " (";
        String valuesString = " VALUES (";
//        System.out.println(insertStatementString);


        List<Object> values = new ArrayList<>();

        int insertedId = -1, index = 1;

        for (Field field : object.getClass().getDeclaredFields()) {
            String header = "";

            field.setAccessible(true); // set modifier to public

            try {
                values.add(field.get(object));
                header = field.getName();
//                System.out.println(field.getNume() + "=" + value);
            } catch (IllegalArgumentException e) {
                e.printStackTrace();
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            }

            if (index != 1) {
                valuesString += ",";
                insertStatementString += ",";
            }

            valuesString += "?";
            insertStatementString += header;


            index++;
        }

        valuesString += ")";
        insertStatementString += ")";
        insertStatementString += valuesString;
        insertStatementString += ";";

//        System.out.println(insertStatementString);

        try {
            insertStatement = dbConnection.prepareStatement(insertStatementString, Statement.RETURN_GENERATED_KEYS);
            index = 0;
//            System.out.println(insertStatementString);
//            System.out.println(insertStatement.toString());
            for (Object value : values) {
                index++;
//                System.out.println();
                if (value instanceof String) {
//                    System.out.println(value.toString());
                    insertStatement.setString(index, value.toString());
                    continue;
                }
//                System.out.println(value.toString());
                insertStatement.setInt(index, Integer.parseInt(value.toString()));
//                System.out.println(value.toString());
            }
//            System.out.println(insertStatement.toString());
            insertStatement.executeUpdate();

            ResultSet rs = insertStatement.getGeneratedKeys();
            if (rs.next()) {
                insertedId = rs.getInt(1);
            }
        } catch (SQLException e) {
            LOGGER.log(Level.WARNING, "UniversalDAO:insert " + e.getMessage());
        } finally {
            ConnectionFactory.close(insertStatement);
            ConnectionFactory.close(dbConnection);
        }
        System.out.println(insertedId);
        return insertedId;
    }

    public static int updateObject(Object object, List<Integer> id) {
        Connection dbConnection = ConnectionFactory.getConnection();
        PreparedStatement updateStatement = null;

        String updateStatementString = "UPDATE " + object.getClass().getSimpleName();
        String setString = " SET ";
        System.out.println(updateStatementString);
        ResultSet rs = null;

        List<Object> values = new ArrayList<>();

        int insertedId = -1, index = 1;

        for (Field field : object.getClass().getDeclaredFields()) {
            String header = "";

            field.setAccessible(true); // set modifier to public

            try {
                values.add(field.get(object));
                header = field.getName();
//                System.out.println(field.getNume() + "=" + value);
            } catch (IllegalArgumentException e) {
                e.printStackTrace();
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            }

            if (index != 1) {
                setString += ",";
            }

            setString += header + "= ?";

            index++;
        }

        updateStatementString += setString;

        String name = getString(object);

        updateStatementString += " WHERE " + name + " = ?";

//        System.out.println(updateStatementString);

        try {
            System.out.println(updateStatementString);
            updateStatement = dbConnection.prepareStatement(updateStatementString, Statement.RETURN_GENERATED_KEYS);
            index = 0;
//            System.out.println(insertStatementString);
//            System.out.println(insertStatement.toString());
            for (Object value : values) {
                index++;
//                System.out.println();
                if (value instanceof String) {
//                    System.out.println(value.toString());
                    updateStatement.setString(index, value.toString());
                    continue;
                }
//                System.out.println(value.toString());
                updateStatement.setInt(index, Integer.parseInt(value.toString()));
//                System.out.println(value.toString());
            }

            index++;
            updateStatement.setInt(index, id.get(0));
            if (object instanceof Iesire) {
                index++;
                updateStatement.setInt(index, id.get(1));
            }
//            System.out.println(insertStatement.toString());
            updateStatement.executeUpdate();

            rs = updateStatement.getGeneratedKeys();
            if (rs.next()) {
                insertedId = rs.getInt(1);
            }
        } catch (SQLException e) {
            LOGGER.log(Level.WARNING, "UniversalDAO:Update " + e.getMessage());
        } finally {
            ConnectionFactory.close(rs);
            ConnectionFactory.close(updateStatement);
            ConnectionFactory.close(dbConnection);
        }
        System.out.println(insertedId);
        return insertedId;
    }

    private static String getString(Object object) {
        String name = "";
        if (object instanceof Client) {
            name += "id_client";
        }
        if (object instanceof Produs) {
            name += "id_produs";
        }
        if (object instanceof Comanda) {
            name += "id_comanda";
        }
        if (object instanceof Iesire) {
            name += "id_comanda = ? AND id_produs";
        }
        return name;
    }

    public static Object findById(Object object, List<Integer> id) {
        Object toReturn = null;

        Connection dbConnection = ConnectionFactory.getConnection();
        PreparedStatement findStatement = null;
        ResultSet rs = null;

        String name = getString(object);
        String findStatementString = "SELECT * FROM " + object.getClass().getSimpleName() + " WHERE " + name + " = ?";

        try {
//            System.out.println(findStatementString);
            findStatement = dbConnection.prepareStatement(findStatementString);
            findStatement.setLong(1, id.get(0));
            if (object instanceof Iesire) {
                findStatement.setLong(2, id.get(1));
            }
            rs = findStatement.executeQuery();
            rs.next();

            List<Object> values = new ArrayList<>();

            int index = 1;


            for (Field field : object.getClass().getDeclaredFields()) {
                values.add(rs.getString(index));
                index++;
            }

            if (object instanceof Client) {
                ((Client) object).setAll(values);
            }
            if (object instanceof Produs) {
                ((Produs) object).setAll(values);
            }
            if (object instanceof Comanda) {
                ((Comanda) object).setAll(values);
            }
            if (object instanceof Iesire) {
                ((Iesire) object).setAll(values);
            }

            toReturn = object;

        } catch (SQLException e) {
            LOGGER.log(Level.WARNING, "UniversalDAO:findById " + object.getClass().getSimpleName() + " " + e.getMessage());
        } finally {
            ConnectionFactory.close(rs);
            ConnectionFactory.close(findStatement);
            ConnectionFactory.close(dbConnection);
        }

        return toReturn;
    }

    public static Object deleteById(Object object, List<Integer> id) {
        Object toReturn = null;

        Connection dbConnection = ConnectionFactory.getConnection();
        PreparedStatement deleteStatement = null;

        String name = getString(object);
        String deleteStatementString = "DELETE FROM " + object.getClass().getSimpleName() + " WHERE " + name + " = ?";

        try {
//            System.out.println(findStatementString);
            deleteStatement = dbConnection.prepareStatement(deleteStatementString);
            deleteStatement.setLong(1, id.get(0));
            if (object instanceof Iesire) {
                deleteStatement.setLong(2, id.get(1));
            }
            deleteStatement.executeUpdate();

            toReturn = object;

        } catch (SQLException e) {
            LOGGER.log(Level.WARNING, "UniversalDAO:RemoveById " + e.getMessage());
        } finally {
            ConnectionFactory.close(deleteStatement);
            ConnectionFactory.close(dbConnection);
        }

        return toReturn;
    }

    public static List<Iesire> selectByIdIesire(int id) {
        List<Iesire> toReturn = new ArrayList<>();

        Connection dbConnection = ConnectionFactory.getConnection();
        PreparedStatement selectStatement = null;
        ResultSet rs = null;
        int index;
        List<Object> values;
        Iesire object = new Iesire();

        String name = "id_comanda";
        String deleteStatementString = "SELECT * FROM iesire WHERE " + name + " = ?";

        try {
//            System.out.println(findStatementString);
            selectStatement = dbConnection.prepareStatement(deleteStatementString);
            selectStatement.setLong(1, id);

            rs = selectStatement.executeQuery();

            while (rs.next()) {

                index = 1;
                values = new ArrayList<>();
                for (Field field : object.getClass().getDeclaredFields()) {
                    values.add(rs.getString(index));
                    index++;
                }

                Iesire object1 = new Iesire();
                object1.setAll(values);
                toReturn.add(object1);
            }
        } catch (SQLException e) {
            LOGGER.log(Level.WARNING, "UniversalDAO:RemoveById " + e.getMessage());
        } finally {
            ConnectionFactory.close(rs);
            ConnectionFactory.close(selectStatement);
            ConnectionFactory.close(dbConnection);
        }

        return toReturn;
    }

    public static ObservableList<Object> viewAll(Object object, TableView<Object> table) {
        Connection dbConnection = ConnectionFactory.getConnection();
        PreparedStatement selectStatement = null;

        String selectStatementString = "SELECT * FROM " + object.getClass().getSimpleName();
        System.out.println(selectStatementString);


        List<Object> values = new ArrayList<>();
        ObservableList<Object> oList = FXCollections.observableArrayList();
        ArrayList<TableColumn> list = new ArrayList<>();

        int insertedId = -1, index = 1;

        for (Field field : object.getClass().getDeclaredFields()) {
            String header = "";

            field.setAccessible(true); // set modifier to public

            try {
                values.add(field.get(object));
                header = field.getName();
                if (field.getType().isInstance("")) {
                    TableColumn<Object, String> t = new TableColumn<>(header.toUpperCase());
                    t.setMinWidth(180);
                    t.setCellValueFactory(new PropertyValueFactory<>(header));
                    list.add(t);
                } else {
                    TableColumn<Object, Integer> t = new TableColumn<>(header.toUpperCase());
                    t.setMinWidth(180);
                    t.setCellValueFactory(new PropertyValueFactory<>(header));
                    list.add(t);
                }
            } catch (IllegalArgumentException e) {
                e.printStackTrace();
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            }

            index++;
        }

        for (TableColumn column : list) {
            table.getColumns().add(column);
        }

        ResultSet rs = null;
        try {
            selectStatement = dbConnection.prepareStatement(selectStatementString, Statement.RETURN_GENERATED_KEYS);
//            System.out.println(insertStatement.toString());
            rs = selectStatement.executeQuery();

            while (rs.next()) {

                index = 1;
                values = new ArrayList<>();
                for (Field field : object.getClass().getDeclaredFields()) {
                    values.add(rs.getString(index));
                    index++;
                }

                if (object instanceof Client) {
                    Client object1 = new Client();
                    object1.setAll(values);
                    oList.add(object1);
                }
                if (object instanceof Produs) {
                    Produs object1 = new Produs();
                    object1.setAll(values);
                    oList.add(object1);
                }
                if (object instanceof Comanda) {
                    Comanda object1 = new Comanda();
                    object1.setAll(values);
                    oList.add(object1);
                }
                if (object instanceof Iesire) {
                    Iesire object1 = new Iesire();
                    object1.setAll(values);
                    oList.add(object1);
                }
            }
        } catch (SQLException e) {
            LOGGER.log(Level.WARNING, "UniversalDAO:Update " + e.getMessage());
        } finally {
            ConnectionFactory.close(rs);
            ConnectionFactory.close(selectStatement);
            ConnectionFactory.close(dbConnection);
        }

        return oList;
    }
}
