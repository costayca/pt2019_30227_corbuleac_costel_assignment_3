package model;

import java.util.List;

/**
 * @Author: Costel Corbuleac
 * @Since: Jun 06, 2019
 */

public class Comanda {
    private int id_comanda;
    private int id_client;

    public Comanda(int id_comanda, int id_client) {
        this.id_comanda = id_comanda;
        this.id_client = id_client;
    }

    public Comanda() {
    }

    public int getId_comanda() {
        return id_comanda;
    }

    public void setId_comanda(int id_comanda) {
        this.id_comanda = id_comanda;
    }

    public int getId_client() {
        return id_client;
    }

    public void setId_client(int id_client) {
        this.id_client = id_client;
    }

    public void setAll(List<Object> values) {
        this.id_comanda = Integer.parseInt((String) values.get(0));
        this.id_client = Integer.parseInt((String) values.get(1));
    }

    @Override
    public String toString() {
        return "Comanda{" +
                "id_comanda=" + id_comanda +
                ", id_client=" + id_client +
                '}';
    }
}
