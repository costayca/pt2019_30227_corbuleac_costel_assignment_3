package model;

import java.util.List;


/**
 * @Author: Costel Corbuleac
 * @Since: Jun 06, 2019
 */

public class Iesire {
    private int id_comanda;
    private int id_produs;
    private int cantitate;

    public Iesire(int id_comanda, int id_produs, int cantitate) {
        this.id_comanda = id_comanda;
        this.id_produs = id_produs;
        this.cantitate = cantitate;
    }

    public Iesire() {
    }

    public int getId_comanda() {
        return id_comanda;
    }

    public void setId_comanda(int id_comanda) {
        this.id_comanda = id_comanda;
    }

    public int getId_produs() {
        return id_produs;
    }

    public void setId_produs(int id_produs) {
        this.id_produs = id_produs;
    }

    public int getCantitate() {
        return cantitate;
    }

    public void setCantitate(int cantitate) {
        this.cantitate = cantitate;
    }

    public void setAll(List<Object> values) {
        this.id_comanda = Integer.parseInt((String) values.get(0));
        this.id_produs = Integer.parseInt((String) values.get(1));
        this.cantitate = Integer.parseInt((String) values.get(2));
    }

    @Override
    public String toString() {
        return "Iesire{" +
                "id_comanda=" + id_comanda +
                ", id_produs=" + id_produs +
                ", cantitate=" + cantitate +
                '}';
    }
}
