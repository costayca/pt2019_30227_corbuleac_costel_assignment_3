package model;

import java.util.List;

/**
 * @Author: Costel Corbuleac
 * @Since: Jun 06, 2019
 */

public class Produs {
    private int id_produs;
    private int pret;
    private String nume;
    private int cantitate;

    public Produs(int id_produs, int pret, String nume, int cantitate) {
        this.id_produs = id_produs;
        this.pret = pret;
        this.nume = nume;
        this.cantitate = cantitate;
    }

    public Produs() {
    }

    @Override
    public String toString() {
        return "Produs{" +
                "id_produs=" + id_produs +
                ", pret=" + pret +
                ", nume='" + nume + '\'' +
                ", cantitate=" + cantitate +
                '}';
    }

    public int getId_produs() {
        return id_produs;
    }

    public void setId_produs(int id_produs) {
        this.id_produs = id_produs;
    }

    public int getPret() {
        return pret;
    }

    public void setPret(int pret) {
        this.pret = pret;
    }

    public String getNume() {
        return nume;
    }

    public void setNume(String nume) {
        this.nume = nume;
    }

    public int getCantitate() {
        return cantitate;
    }

    public void setCantitate(int cantitate) {
        this.cantitate = cantitate;
    }

    public void setAll(List<Object> values) {
        this.id_produs = Integer.parseInt((String) values.get(0));
        this.pret = Integer.parseInt((String) values.get(1));
        this.nume = (String) values.get(2);
        this.cantitate = Integer.parseInt((String) values.get(3));
    }
}
