package model;

import java.util.List;

/**
 * @Author: Costel Corbuleac
 * @Since: Jun 06, 2019
 */

public class Client {
    private int id_client;
    private String nume;
    private String adresa;
    private String email;
    private int varsta;

    public Client(int id_client, String name, String adresa, String email, int varsta) {
        this.id_client = id_client;
        this.nume = name;
        this.adresa = adresa;
        this.email = email;
        this.varsta = varsta;
    }

    public Client() {

    }

    public int getId_client() {
        return id_client;
    }

    public void setId_client(int id_client) {
        this.id_client = id_client;
    }

    public String getNume() {
        return nume;
    }

    public void setNume(String nume) {
        this.nume = nume;
    }

    public String getAdresa() {
        return adresa;
    }

    public void setAdresa(String adresa) {
        this.adresa = adresa;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public int getVarsta() {
        return varsta;
    }

    public void setVarsta(int varsta) {
        this.varsta = varsta;
    }

    public void setAll(List<Object> values) {
        this.id_client = Integer.parseInt((String) values.get(0));
        this.nume = (String) values.get(1);
        this.adresa = (String) values.get(2);
        this.email = (String) values.get(3);
        this.varsta = Integer.parseInt((String) values.get(4));
    }

    @Override
    public String toString() {
        return "Client{" +
                "id_client=" + id_client +
                ", nume='" + nume + '\'' +
                ", adresa='" + adresa + '\'' +
                ", email='" + email + '\'' +
                ", varsta=" + varsta +
                '}';
    }
}
