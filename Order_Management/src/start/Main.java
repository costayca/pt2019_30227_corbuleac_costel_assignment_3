package start;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.text.Font;
import javafx.stage.Stage;

/**
 * @Author: Costel Corbuleac
 * @Since: Jun 06, 2019
 */

public class Main extends Application {

    @Override
    public void start(Stage primaryStage) throws Exception{
        Font.loadFont(getClass().getResource("/fonts/VarelaRound-Regular.ttf").toExternalForm(), 10);
        Parent root = FXMLLoader.load(getClass().getResource("/fxml/Client.fxml"));
        root.getStylesheets().add(getClass().getResource("/css/main.css").toExternalForm());
        primaryStage.setTitle("Client Window");
        primaryStage.setScene(new Scene(root, 800, 600));
        primaryStage.show();

        FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/Produs.fxml"));
        Stage newStage = new Stage();
        Parent root1 = loader.load();
        root1.getStylesheets().add(getClass().getResource("/css/main.css").toExternalForm());
        newStage.setScene(new Scene(root1, 800, 600));
        newStage.setTitle("Product Window");
        newStage.show();

        FXMLLoader loader2 = new FXMLLoader(getClass().getResource("/fxml/Comanda.fxml"));
        Stage newStage2 = new Stage();
        Parent root2 = loader2.load();
        root2.getStylesheets().add(getClass().getResource("/css/main.css").toExternalForm());
        newStage2.setScene(new Scene(root2, 800, 600));
        newStage2.setTitle("Order Window");
        newStage2.show();

        FXMLLoader loader3 = new FXMLLoader(getClass().getResource("/fxml/Iesire.fxml"));
        Stage newStage3 = new Stage();
        Parent root3 = loader3.load();
        root3.getStylesheets().add(getClass().getResource("/css/main.css").toExternalForm());
        newStage3.setScene(new Scene(root3, 800, 600));
        newStage3.setTitle("Order Details Window");
        newStage3.show();
    }


    public static void main(String[] args) {
        launch(args);
    }
}
